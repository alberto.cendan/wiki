#  Python OOP
## Clases y métdos
Al igual que en otros lenguajes de programación, php utiliza la programación orientada a objetos.

Su sintáxis es parecida al resto de lenguajes, como se puede ver en el siguiente código 
```php
<?php

  

class Person{


	public $firstName; # Se declaran variables públicas
	public $lastName;
	public $gender;

  
	# Se declara el constructor 
	public function __construct($firstName,$lastName,$gender = 
    "f")

	{
	# Se asignan las variables 
	$this -> firstName = $firstName;
	$this -> lastName = $lastName;
	$this-> gender = $gender;

	}

	public function fullName() # Método que devuelve el fullname
	
	{
		return $this->firstName." ".$this->lastName;
	}

}


$tom = new Person("Tom", "Ben","m");
$jane = new Person("Jane","Ben");

echo $tom->fullName(); # Tom Ben
echo "\n";
echo $tom->firstName; # Tom
echo "\n";
echo $jane->fullName(); # Jane Ben
```

## Extends y parent::__construct()
Al usar extends en una class hacemos que esta **inherentemente tenga las propiedades de la clase que extiende.**

Por ejemplo si creamos la clase Employee que extiende a Person, **Employee va a tener todas y cada una de las propiedades que tenía Person,** más las que le pongamos a mayores.


```php

# Usamos extends para inherir las propiedades de Person en Employee

class Employee extends Person {

	public $jobTitle;
	public function 
    __construct($jobTitle,$firstName,$lastName,$gender = "f")

	# Inicializamos el constructor declarando en los parámetros 
    # todas las variables necesarias
    {

		# Usamos parent::__construct() para asignar las 
        # variables provenientes de Person
        
		$this->jobTitle = $jobTitle;
		parent::__construct($firstName,$lastName,$gender);

	}

public function getJobTitle() {

return $this->jobTitle;

}

}
class Person{


	public $firstName; # Se declaran variables públicas
	public $lastName;
	public $gender;

  
	# Se declara el constructor 
	public function __construct($firstName,$lastName,$gender = 
    "f")

	{
	# Se asignan las variables 
	$this -> firstName = $firstName;
	$this -> lastName = $lastName;
	$this-> gender = $gender;

	}

	public function fullName() # Método que devuelve el fullname
	
	{
		return $this->firstName." ".$this->lastName;
	}

}


$jane = new Employee("Backend Developer","Jane","Bob");
echo $jane->getJobTitle(); # Backend Developer
echo "\n";
echo $jane->getGender(); # f

```

## Visibility Scopes, getter y setters
Existen **3 tipos diferentes de scopes**

- **Public**
- **Private**
- **Protected**

Dependiendo del scope que le otorguemos a nuestra variable/método, **tendrá un comportamiento u otro** a la hora de acceder a la misma.

### Public

Cuando asignamos la scope Public, **podremos acceder y modificar las propiedades y métodos desde fuera de la scope de la  clase**, es decir, una instancia podría modificar sus propiedades sin necesidad de llamar a la class en sí.

```php
$jane->jobTitle = "Frontend Developer"
```

### Private
Cuando asignamos **la scope Private**, **sólo podremos acceder y modificar las propiedades y métodos desde dentro de la scope de clase.**

**Ni si quiera las child classes podrán acceder a esa propiedad**

**Para modificar entonces estos parámetros se utilizan métodos setter**

```php
# Usando el método setter cambiamos el valor de la variable

public function setJobTitle($jobTitle) {
	$this->jobTitle = $jobTitle
}

$jane->setJobTitle("Frontend Developer");
echo $jane->jobTitle; # Esta línea dará error, ya que estamos intentando acceder al valor desde la instancia y no desde la clase
```

**Para acceder a los valores se utilizan métodos getter**

```php
public function getJobTitle() {
	return $this->jobTitle;
}

echo $jane->getJobTitle(); # Frontend Developer
```

Esto **se utiliza cuando queremos recorrer la lógica de la clase y de los métodos que tenemos definidos**, por ejemplo, si necesitásemos que el jobTitle tuviese un formato en específico.

### Protected 
Cuando asignamos la scope protected **estamos prácticamente asignando la scope private,** salvo que con protected **las classes inherentes pueden acceder a la propiedad.**

## __ set y __ get
Ahora que entendemos las Visibility Scopes, nos surge un problema: **por cada propiedad a la que le asignemos private o protected, necesitamos 2 métodos get y set.**

Imagínate que tienes 10 variables protegidas con private, necesitarías 20 métodos.

Para solucionar esto, **existen los métodos** _____set y __get

### Set
```php

class Employee extends Person {

	private $jobTitle; # jobTitle es privado
	public function 
    __construct($jobTitle,$firstName,$lastName,$gender = "f")

    {    
		$this->jobTitle = $jobTitle;
		parent::__construct($firstName,$lastName,$gender);
	}

	# Declarando el método __set, cada vez que intentemos 
    # cambiar el valor de un atributo de cualquier instancia  
    # de Employee, ejecutaremos el método setter, pudiendo 
    # cambiar así el valor de la variable
         
	public function __set($name,$value) {
		$this->$name = $value;
	}

public function getJobTitle() {

return $this->jobTitle;

}

}
class Person{


	public $firstName; 
	public $lastName;
	public $gender;

	public function __construct($firstName,$lastName,$gender = 
    "f")

	{

	$this -> firstName = $firstName;
	$this -> lastName = $lastName;
	$this-> gender = $gender;

	}

	public function fullName() # Método que devuelve el fullname
	
	{
		return $this->firstName." ".$this->lastName;
	}

}

$jane->$jobTitle = "Tester" # Funciona por que tenemos definido el setter, si no daría un error ya que intentamos acceder desde una instancia a una variable definida como private

echo $jane->getJobTitle() # Tester
```

### Get
```php

class Employee extends Person {

	private $jobTitle; # jobTitle es privado
	public function 
    __construct($jobTitle,$firstName,$lastName,$gender = "f")

    {    
		$this->jobTitle = $jobTitle;
		parent::__construct($firstName,$lastName,$gender);
	}

	public function __set($name,$value) {
		$this->$name = $value;
	}
# El método get funciona de manera similar al set, cada vez que intentemos acceder a una propiedad privada o protected directamente se ejecuta el __get y nos devuelve el valor
	public function __get($name) {
		return $this->$name
	}
public function getJobTitle() {

return $this->jobTitle;

}

}
class Person{


	public $firstName; 
	public $lastName;
	public $gender;

	public function __construct($firstName,$lastName,$gender = 
    "f")

	{

	$this -> firstName = $firstName;
	$this -> lastName = $lastName;
	$this-> gender = $gender;

	}

	public function fullName() # Método que devuelve el fullname
	
	{
		return $this->firstName." ".$this->lastName;
	}

}

$jane->$jobTitle = "Tester"
echo $jane->jobTitle # Funciona por que está el getter definido, si no devolvería un error.
```