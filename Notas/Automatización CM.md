# Automatización CM
**El CM es una Spreadsheet donde se guardan datos** relacionados con los posts, urls, keywords ....

### Automatizar Keywords
* Las keywords y keywords relacionadas se introducen en el CM y **se debe obtener el volumen de búsqueda** de cada una de ellas. 
* Una vez obtenido el volumen **se debe pintar en la columna volumen** y fila correspondiente

### Posición media de las Keywords
* Debemos obtener la posición media de las Keywords ( Utilizando Google Search Console) y volcarlo en el CM

### Fragmento destacado o FS/FD
* **Mediante el URL** del post , **marcar o no un check** en función de si ese post en concreto **tiene o no Fragmento Destacado**. 
* Este fragmento destacado **debe coincidir con el URL del post** (Lo normal)

### Comprobar las Keywords que generan o no Fragmentos Destacados

### Bugs
* En el CM no aparece el URL home (/) de las verticales.