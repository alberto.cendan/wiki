# Pandas

## Creating Reading & Writing
Pandas es una librería muy popular dedicada al análisis de datos.

Existen **dos objetos principales** en pandas, el **DataFrame** y las **Series**

### DataFrame
**Un DataFrame es una tabla,** contiene una array de registros individuales, cada uno de los cuales contiene un valor.

**Cada registro corresponde a una fila y columna.**

Por ejemplo:

```python
# Input

pd.DataFrame({'Yes': [50, 21], 'No': [131, 2]})

# Output

|   | Yes | No 
| 0 | 50 | 131
| 1 | 21 | 2
```

Usamos el constructor pd.DataFrame() para generar los DataFrame objects.

La sitáxis para generar nuevos objetos **se basa en un diccionario cuyas claves son los nombres de las columnas, y sus valores son los registros** de las mismas.

El constructor asigna de manera automática indexes empezando desde 0, esto está bien para algunos casos, pero **si deseamos poner otros índices, podemos agregar el parámetro index en el constructor**

```python
pd.DataFrame({'Bob': ['I liked it.', 'It was awful.'], 
              'Sue': ['Pretty good.', 'Bland.']},
             index=['Product A', 'Product B'])
```

## Series
A lo contrario que el objeto DataFrame, las Series son una secuencia de valores. Si el DataFrame es una tabla, una Series es una lista.

```python
# Input

pd.Series([1, 2, 3, 4, 5])

# Output

0    1
1    2
2    3
3    4
4    5

dtype: int64
```

**En esencia, una Series es una columna de un DataFrame**. **Puedes agregar labels (indexes)** de la misma manera que en los DataFrames, pero **las Series no tienen column name,** simplemente tienen un overall name.

```python
# Input

pd.Series([30, 35, 40], index=['2015 Sales', '2016 Sales', '2017 Sales'], name='Product A')

# Output

2015 Sales    30
2016 Sales    35
2017 Sales    40
Name: Product A, dtype: int64
```

**Las Series y los DataFrames van de la mano, una manera de verlo es que los DataFrames son Series pegadas unas con otras.**

## Leer data files
Muchas veces estaremos trabajando con datos que ya existen, y estos tendrán diferentes formatos, sin embargo u**no de los formatos más extendidos es el CSV**

```python
Product A,Product B,Product C,
30,21,9,
35,34,1,
41,11,11
```

Podemos leer csv files mediante la función **pd.read_csv()**

```python
mi_csv = pd.read_csv("archivo_datos.csv")
```

Si usamos el atributo **shape** podemos comprobar cuán largo es el archivo

```python
# Input

mi_csv.shape

# Output

(129971, 14) # 130.000 filas a lo largo de 14 columnas diferentes
```

Podemos examinar los contenidos del DataFrame resultante utilizando la propiedad **head()**, que n**os devuelve las 5 primeras filas**

```python
# Input

mi_csv.head()

# Output

(Tabla con 5 filas)
```

El parámetro **pd.read_csv()** está acompañado de más de 30 parámetros opcionales con multitud de usos. 

Por ejemplo si queremos usar una columna existente como index:

```python
# Estaríamos usando la columna 0 como index
mi_csv = pd.read_csv("archivo_datos.csv", index_col=0)
```

## Indexing Selecting & Assigning
Muchas de las propiedades que tiene python, se extienden a Pandas, por ejemplo, si quisiésemos acceder a l**a propiedad title** de un **objeto book**, lo llamaríamos por **book.title**. En Pandas es igual.

Si queremos acceder a la columna **country** del DataFrame **reviews** -> 

```python
reviews.country
```

También podemos acceder como si de un diccionario se tratase ->

```python
reviews["country"]
```

Ambas maneras son válidas, pero los corchetes tienen una ventaja, y es que podemos escribir palabras separadas a diferencia de con el punto.

También podemos acceder a los valores de dentro de la tabla tal que así ->

```python
reviews["country"][0] # Accedemos al registro con index 0 de la columna country
```


### Indexing *loc* e *iloc*
Pandas usa dos access operators, **loc** e **iloc**

#### iloc (Index-based selection)
**iloc nos permite usar una selección basada en índices** que sigue la estructura f**ila-columna** siempre. Esto significa que será mucho más sencillo acceder a una fila que a una columna, por ejemplo ->

```python
# Nos devuelve la primera fila del dataframe reviews

reviews.iloc[0]

```

```python
# Seleccionar la primera columna del dataframe reviews

reviews.iloc[:,0]
```

El parámetro **:** significa "everything", aun que cuando lo siguen otros parámetros puede servir para setear rangos ->

```python
# Seleccionar las primeras tres filas de la primera columna

reviews.iloc[:3, 0]
```

Podemos también pasar una lista, con lo cual el código de arriba sería equivalente al siguiente ->

```python
# Seleccionar las primeras tres filas de la primera columna

reviews.iloc[[0,1,2], 0]
```

Finalmente, no está de más mencionar que los índices negativos pueden sernos útiles, ya que dan la vuelta al dataframe y cuentan desde abajo de la tabla.

Por ejemplo ->

```python
# Seleccionar las últimas 5 filas

reviews.iloc[-5]
```

#### loc (label-based selection)
A diferencia de iloc, que se basaba en el índice para localizar el elemento, **loc toma en cuenta el valor del elemento en sí para localizarlo.**

Por ejemplo para obtener el primer registro ->

```python
reviews.loc[0, 'country']
```

Teniendo en cuenta que tenemos indexes ordenados de 0 a X y que country es la primera columna.

Si iloc tomaba los indexes de cada uno de los elementos, loc toma los valores dentro de estos índices.

Tanto loc como iloc tienen sus pros y sus contras y debemos de elegirlos dependiendo de la tarea que queramos llevar a cabo.

Esta operación, por ejemplo, es mucho más fácil de solventar usando loc

```python
# Selecciona todos las filas para las columnas 'taster_name', 'taster_twitter_handle', 'points']

reviews.loc[:, ['taster_name', 'taster_twitter_handle', 'points']]
```

Otra cosa a tener en cuenta es que **iloc trata los índices de manera exclusiva mientras que loc los trata de manera inclusiva**.

### Manipulando el Index
Cuando usemos loc (label-based selection), hay que entender que **su fuerza reside en los valores que contiene nuestro índice.**

**Nuestro índice es mutable,** es decir, que si encontramos un campo que sirva al propósito de índice mejor que el que ya tenemos asignado, **podemos setearlo como nuevo index** con el método **set_index()**

```python
# Setear la columna title como índice

reviews.set_index("title")
```

### Conditional selection
A veces necesitamos seleccionar ciertos datos basados en una serie de condiciones,  por ejemplo ->

```python
reviews.country == 'Italy'
```

Esto nos printeará True o False dependiendo de si el registro contiene Italy o no, lo que es muy útil a la hora de escribir código.

Imaginémonos que tenemos una tabla de reviews de vino, y queremos seleccionar todos los vinos a los que hemos hecho una review provenientes de Italia, y que tengan una puntuación de más de 90 -> 

```python
reviews.loc[(reviews.country == 'Italy') & (reviews.points >= 90)]
```

La pipe ( | ) funciona como un OR lógico ->

```python
reviews.loc[(reviews.country == 'Italy') | (reviews.points >= 90)]
```

Pandas viene con un par de built-in conditionals, un ejemplo es **is in** que nos permite comprobar si ciertos valores están en una lista ->

```python
reviews.loc[reviews.country.isin(['Italy', 'France'])]
```

Otro ejemplo serían **isnull y notnull**, que nos permite highlightear los valores que están o no vacíos (NaN)

```python
reviews.loc[reviews.price.notnull()]
```

### Asigning Data

Asignar datos a un DataFrame es fácil, podemos asignar un valor fijo ->

```python
reviews['critic'] = 'everyone'
```

o una serie de valores iterables ->

```python
reviews['index_backwards'] = range(len(reviews), 0, -1)
```


## Summary Functions and Maps
### Summary Functions
Pandas cuenta con ciertas funciones muy útiles que nos permiten obtener o formatear información.

#### describe()
Nos sirve para calcular datos estadísticos como el percentile o la media ->

```python
# Input

a1 = pd.Series([1, 2, 3]) 
a1.describe()

# Output

count     3.0
mean      2.0
std       1.0
min       1.0
25%       1.5
50%       2.0
75%       2.5
max       3.0

dtype: float64
```

#### unique()
Nos devuelve una lista con valores únicos ->

```python
# Input

reviews.taster_name.unique()

# Output

array(['Kerin O’Keefe', 'Roger Voss', 'Paul Gregutt',
       'Alexander Peartree', 'Michael Schachner', 'Anna Lee C. Iijima',
       'Virginie Boone', 'Matt Kettmann', nan, 'Sean P. Sullivan',
       'Jim Gordon', 'Joe Czerwinski', 'Anne Krebiehl\xa0MW',
       'Lauren Buzzeo', 'Mike DeSimone', 'Jeff Jenssen',
       'Susan Kostrzewa', 'Carrie Dykes', 'Fiona Adams',
       'Christina Pickard'], dtype=object)
```

#### value_counts()
Para ver el número de ocurrencias de un set de valores únicos podemos usar value_counts() ->

```python
# Input

reviews.taster_name.value_counts()


# Output

Roger Voss           25514
Michael Schachner    15134
                     ...  
Fiona Adams             27
Christina Pickard        6
Name: taster_name, Length: 19, dtype: int64
```

### Maps y apply()

Usando apply() podemos pasar como parámetro cada una de las rows o columnas a la función que nosotros queramos.

Para crear nuevas series, es común hacer lo siugiente ->

```python
# Le decimos que es

def starRating(column)->int:
	if column.country == "Canada" : return 3
	if column.points > 95 : return 3
	elif column.points >= 85 and column.points < 95 : return 2
	else : return 1

reviews.apply(starRating,"columns")

# Esto le pasará a la función cada uno de los registros de las columnas, después de procesarlo podemos asignarlo a una serie y utilizndo (por ejemplo) reviews["stars"] = nuestraSerie agregaremos una nueva columna.

```

Los maps hacen cosas (ver cuando sea necesario)

## Grouping and Sorting
### groupby()
Nos permite agrupar nuestros datos de la manera en que queramos, por ejemplo ->

```python
# Input

reviews.groupby('points').price.min()

# Output

points
80      5.0
81      5.0
       ... 
99     44.0
100    80.0
Name: price, Length: 21, dtype: float64
```

### Uso de agg()
agg() sirve para agrupar más de una función en un DataFrame de manera simultánea ->

```python
reviews.groupby(['country']).price.agg([len, min, max])


# Output

		len min max
country 3800 4.0 230
Argentina ... ... ...
Armenia ...  ... ...
```

### Multiindexes
ver cuando sea necesario

## Data Types and Missing Values
### Dtype
El tipo de dato para una columna en un DataFrame o una Series se conoce como **dtype** ->
```python
# Input

reviews.price.dtype

# Output

dtype('float64')
```

#### Dtypes
Devuelve el dtype de cada columna en el DataFrame ->

```python
# Input

reviews.dtypes

# Output

country        object
description    object
                ...  
variety        object
winery         object
Length: 13, dtype: object
```

#### Conversión de types con astype()
Siempre y cuando una conversión tenga sentido, podemos llevarla a cabo con la función **astype()** ->

```python
reviews.points.astype('float64')

# Pasa de int64 a float64
```

### Missing data
Cuando hay registros que no tienen valor, estos toman el valor NaN (NotaNumber), y por defecto son de tipo float64

Si quisiésemos seleccionar todos los registros sin valores podemos hacer lo siguiente ->

```python
reviews[pd.isnull(reviews.country)]
```

#### fillna()
Una operación muy común es reemplazar todos los registros con valor NaN, y Pandas para ello, nos proporciona la función **fillna()** ->

```python
reviews.region_2.fillna("Unknown")

# Cambia todos los valores NaN de region_2 a "Unknown"

```

#### replace()
Si, en cambio lo que queremos es reemplazar un valor que no es NaN, podemos usar **replace()** ->

```python
reviews.taster_twitter_handle.replace("@kerinokeefe", "@kerino")
```

## Renaming and Combining
### Renaming
Cuando queremos cambiar el nombre de nuestras columnas o índices podemos usar **rename()** ->

```python
# Por ejemplo, si queremos cambiar la columna points a score

reviews.rename(columns={'points': 'score'})

```

**rename()** también permite renombrar valores del índice o columna mediante los parámetros **index={} o columns={}** y pasándole un diccionario ->

```python
reviews.rename(index={0: 'firstEntry', 1: 'secondEntry'})
```

Normalmente renombraremos columnas muy amenudo, para ello , **set_index()** nos es muy útil.

También podemos cambiar el nombre del index de las rows y las columns mediante **rename_axis()** ->

```python
reviews.rename_axis("wines", axis='rows').rename_axis("fields", axis='columns')
```

### Combining

### concat()
Dada una lista de elementos, la función juntará todos ellos en un axis, lo cual es útil si tienes d**iferentes DataFrames o Series pero tienen las mismas columnas** -> 

```python
canadian_youtube = pd.read_csv("../input/youtube-new/CAvideos.csv")
british_youtube = pd.read_csv("../input/youtube-new/GBvideos.csv")

pd.concat([canadian_youtube, british_youtube])
```

### join()
Nos permite combinar distintos DataFrames que tengan un index en común ->

```python
left = canadian_youtube.set_index(['title', 'trending_date'])
right = british_youtube.set_index(['title', 'trending_date'])

left.join(right, lsuffix='_CAN', rsuffix='_UK')
```
