## CLASES & INSTANCES

- Datos asociados con clases -> **atributos**
- Funciones asociadas con una clase -> **método** 
- Una **class** es un **"plano"** de los **objetos / instancias que queremos construir**

```python
class Employee:

  

# self es, por convencion, el nombre que se usa para referirse al objeto o instancia que queremos construir, siempre se pasa como primer argumento

	def __init__(self, firstName, lastName, pay):

		self.firstName = firstName
		self.lastName = lastName
		self.pay = pay
		self.email = firstName + "." + lastName + "@company.com"
# También podemos construir métodos, que nos permitan acceder, por ejemplo, al fullname del employee, utilizando self como argumento
	def fullname(self):
		return "{} {}".format(self.firstName,self.lastName)

# Ahora, podemos asignar una variable employee1 que se construiŕa con todas aquellas variables que hayamos definido

employee1 = Employee("Pepe", "Pepi", 2000)

  

print(employee1.email) # Pepe.Pepi@company.com

print(employee1.fullname()) # Pepe Pepi
```

## Class Variables
* Podemos definir variables de clase y así hacer que **todas y cada una de las instancais que creemos puedan acceder a ellas.**
* De esa manera **evitamos tener que recorrer todo el código** para cambiar un solo valor.

```python 

class Employee:

# Aquí podemos definir las variables de clase

	number_of_emp = 0
	raise_amount = 1.04

	def __init__(self, firstName, lastName, pay):

		self.firstName = firstName
		self.lastName = lastName
		self.pay = pay
		self.email = firstName + "." + lastName + "@company.com"

# Usamos Employee en vez de self por que no existe caso útil para que una instancia pueda modificar el valor de la variable.

		Employee.number_of_emp =+ 1

# Cada vez que creemos una instancia, el constructor se inicia y agrega +1 a la variable number_of_emp, a la que podremos acceder cuando queramos para retornar el número total de empleados

	def fullname(self):
		return "{} {}".format(self.firstName,self.lastName)

# Utilizamos la variable de clase raise_amount para aplicar la subida de salario.

	def apply_raise(self):
		self.pay = int(self.pay * self.raise_amount)

	
print(Employee.number_of_emp) # 0

employee1 = Employee("Pepe","Pepi",2000) 

print(employee1.pay) # 2000


print(employee1.pay) # 2080

print(Employee.number_of_emp) # 1

```

## @classmethods & @staticmethods

### @classmethods

* Los @classmethods **son métodos a los que en vez de pasarle la instancia como parámetro se le pasa la clase en sí**:

```python
class Employee:

	number_of_emp = 0
	raise_amount = 1.04 # Definimos una variable raise_amount

	def __init__(self, firstName, lastName, pay):

		self.firstName = firstName
		self.lastName = lastName
		self.pay = pay
		self.email = firstName + "." + lastName + "@company.com"

		Employee.number_of_emp =+ 1

	def fullname(self):
		return "{} {}".format(self.firstName,self.lastName)

	def apply_raise(self):
		self.pay = int(self.pay * self.raise_amount)


# Así como por convención se pasa el parámetro self en los métodos, en los métodos de clase pasaremos cls, ya que class es una palabra reservada en Python.

	@classmethod 
	def set_raise_amount(cls, amount):
		cls.raise_amt = amount

employee1 = Employee("Pepe","Pepi",2000)
employee2 = Employee("David","Perez",1000)

print(Employee.raise_amount)  # 1.04
print(employee1.raise_amount) # 1.04
print(employee1.raise_amount) # 1.04

  

Employee.set_raise_amount(1.05) # Mediante el classmethod accedemos a la función set_raise_amount y cambiamos para todas las instancias el valor del raise_amount

  

print(Employee.raise_amount) # 1.05
print(employee1.raise_amount) # 1.05
print(employee1.raise_amount) # 1.05

```

#### Real World Scenario
Vamos a ver un caso del mundo real, en el que nos llega una string en cierto formato y la pasaremos como argumento para crear un employee

```python
class Employee:

	number_of_emp = 0
	raise_amount = 1.04 

	def __init__(self, firstName, lastName, pay):

		self.firstName = firstName
		self.lastName = lastName
		self.pay = pay
		self.email = firstName + "." + lastName + "@company.com"

		Employee.number_of_emp =+ 1

	def fullname(self):
		return "{} {}".format(self.firstName,self.lastName)

	def apply_raise(self):
		self.pay = int(self.pay * self.raise_amount)

	@classmethod 
	def set_raise_amount(cls, amount):
		cls.raise_amt = amount

# Creamos el classmethod que parseará la string y la asignará a las 3 variables correspondientes.

	@classmethod
	def emp_from_string(cls,emp_str):
		firstName, lastName, pay = emp_str.split("-") 
		pay = int(pay) 

# Si queremos seguir calculando el pay raise debemos cambiar la string pay por un integer, ya que por defecto al hacer el split se guarda como string.

# Creamos la instancia

		cls(firstName, lastName, pay)

emp_str_1 = "John-Doe-2000"

emp1 = Employee.emp_from_string(emp_str_1)

print(emp1.pay) # 2000
emp1.apply_raise()
print(emp1.pay) # 2080
```

#### @staticmethods

A diferencia de los métodos normales, o los classmethods, **los staticmethods no reciben ni la instancia ni la clase como parámetro.**

**Es decir, son funciones normales** y corrientes, **que se ubican dentro de la clase por que tienen alguna relación con ella de una manera u otra.**

```python
class Employee:

	number_of_emp = 0
	raise_amount = 1.04 

	def __init__(self, firstName, lastName, pay):

		self.firstName = firstName
		self.lastName = lastName
		self.pay = pay
		self.email = firstName + "." + lastName + "@company.com"

		Employee.number_of_emp =+ 1

	def fullname(self):
		return "{} {}".format(self.firstName,self.lastName)

	def apply_raise(self):
		self.pay = int(self.pay * self.raise_amount)

	@classmethod 
	def set_raise_amount(cls, amount):
		cls.raise_amt = amount


	@classmethod
	def emp_from_string(cls,emp_str):
		firstName, lastName, pay = emp_str.split("-") 
		pay = int(pay) 
		cls(firstName, lastName, pay)

	@staticmethod
	def is_workday(day):
		if day.weekday() == 5 or day.weekday() == 6:
			return False
		return True


import datetime

  

my_date = datetime.date(2022, 3, 4) # Viernes
my_date2 = datetime.date(2022, 3, 5) # Sábado

  

print(Employee.is_workday(my_date)) # True
print(Employee.is_workday(my_date2)) # False
```

## Class Inheritance
La inheritancia **nos permite inherir propiedades de las clases padre**, esto es muy útil ya que podemos crear **subclases que compartan las mismas propiedades que la clase padre**, y luego **modificar su comportamiento o implementar nuevas funcionalidades.**


```python
class Employee:

	number_of_emp = 0
	raise_amount = 1.04 

	def __init__(self, firstName, lastName, pay):

		self.firstName = firstName
		self.lastName = lastName
		self.pay = pay
		self.email = firstName + "." + lastName + "@company.com"

		Employee.number_of_emp =+ 1

	def fullname(self):
		return "{} {}".format(self.firstName,self.lastName)

	def apply_raise(self):
		self.pay = int(self.pay * self.raise_amount)


class Developer(Employee): # Ahora Developer tendrá todos los atributos y métodos de la clase Employee
	raise_amount = 1.10
# En cuanto sobreescribimos la raise_amount , se convierte en 1.10 únicamente para los developers, de lo contrario, compartiría la raise amount del resto de Employees (1.04)
	
dev_1 = Developer("Corey","Schafer",5000) # 5000
dev_2 = Developer("Tanio","Andamio",4000) # 5500

print(dev_1.pay) # 5000
dev_1.apply_raise()
print(dev_1.pay) # 5500

```

### Uso de super() y demás

Cuando trabajamos con subclases, podemos usar super() para asignar las variables de la clase padre automáticamente a la hija.

También podemos usar los métodos isinstance y issubclass para comprobar si pertenece a una instancia o una subclase


```python
class Employee:

	number_of_emp = 0
	raise_amount = 1.04 

	def __init__(self, firstName, lastName, pay):

		self.firstName = firstName
		self.lastName = lastName
		self.pay = pay
		self.email = firstName + "." + lastName + "@company.com"

		Employee.number_of_emp =+ 1

	def fullname(self):
		return "{} {}".format(self.firstName,self.lastName)

	def apply_raise(self):
		self.pay = int(self.pay * self.raise_amount)


class Developer(Employee): 

	raise_amount = 1.10
	def __init__(self, first, last, pay, prog_lang):
		super().__init__(first,last,pay)
		self.prog_lang = prog_lang

# Ahora la clase Developer tiene las propiedades de Employee, más la variable prog_lang
	

dev_1 = Developer("Corey","Schafer",5000) # 5000
dev_2 = Developer("Tanio","Andamio",4000) # 5500

isinstance(dev_1, Developer) # True
issubclass(Developer, Employee) # True
print(dev_1.pay) # 5000
dev_1.apply_raise()
print(dev_1.pay) # 5500

```